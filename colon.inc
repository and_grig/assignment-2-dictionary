%define ZERO_IND 0
%macro colon 2
  %2:
  dq ZERO_IND
  db %1, 0
  %define ZERO_IND %2
%endmacro
