section .text

%include "lib.inc"

global find_word
find_word:
  mov rdx, rsi
  .loop:
    add rsi, 8
    push rdi
    push rsi
    call string_equals
    pop rsi
    pop rdi
    sub rsi, 8
    test rax, rax
    jnz .return
    mov rsi, [rsi]
    test rsi, rsi
    jz .return_zero
    jmp .loop

  .return:
    mov rax, rsi
    ret

  .return_zero:
    xor rax, rax
    ret
