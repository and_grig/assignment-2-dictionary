%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUFFER_SIZE 256

section .rodata
overflow: db "Buffer overflow", 10, 0
not_found: db "Not found", 10, 0

section .bss
buffer: resb BUFFER_SIZE

section .text
global _start
_start:
  mov rdi, buffer
  mov rsi, BUFFER_SIZE
  call read_word
  test rax, rax
  jz .buffer_error

  mov rdi, rax
  mov rsi, ZERO_IND
  call find_word
  test rax, rax
  jz .not_found

  add rax, 8
  mov rdi, rax
  push rdi
  call string_length
  pop rdi
  add rdi, rax
  inc rdi
  call print_string
  call print_newline
  call exit

.not_found:
  mov rdi, not_found
  call print_error
  call exit

.buffer_error:
  mov rdi, overflow
  call print_error
  call exit
