ASM=nasm
FLAGS = -f elf64 
MAIN=main

%.o: %.asm
	$(ASM) $(FLAGS) $< -o $@ 

$(MAIN): $(MAIN).o dict.o lib.o
	ld $^ -o $@

.PHONY: clean
clean:
	rm *.o $(MAIN)
